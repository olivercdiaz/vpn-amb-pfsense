# VPN amb PfSense

Vamos a entrar a ubuntu desktop con la ip LAN de pfSense

![](images/1.png)
![](images/2.png)
Usuario: admin
Contraseña: pfSense

Como lo he instalado de nuevo vamos a configurarlo.

![](images/3.png)

**Configuración OPENVPN con Wizard**

![](images/4.png)

1. Configurar el Certificado

![](images/5.png)

2. Información del OPEN VPN Server

![](images/6.png)

3. Añadir LA IP y donde accederá. "192.168.1.0" que es la red LAN

![](images/7.png)

4. Para que el firewall se cree automáticamente las reglas marcamos las dos siguientes opciones

![](images/8.png)

5. Ya estará configurado.

![](images/9.png)

Ahora vamos a añadir usuarios.

Oliver-VPN y ponemos una contraseña, marcamos que se cree un Certificado

![](images/11.png)
![](images/12.png)
![](images/13.png)

Vamos a Package Manager y buscamos el paquete que dice "Client-Export" e instalamos.

![](images/10.png)

**Reglas Firewall**

1. Añadimos las siguiente reglas

![](images/14.png)

**Creación de usuario**

![](images/15.png)
![](images/16.png)

Al crearse el usuario, se mostrará en el apartado User, donde antes solo estaba "admin."

Ahora iremos a "Package Manager" para instalar un paquete de certificados.

![](images/17.png)

Descargamos en el pfsense el OPENVPN y lo insertaomos en nuestra máquina anfitrión.

![](images/18.png)

![](images/19.png)

![](images/20.png)

**PRUEBAS**

![](images/21.png)

![](images/22.png)

![](images/23.png)
